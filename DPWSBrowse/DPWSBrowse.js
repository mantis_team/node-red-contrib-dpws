module.exports = function(RED){
    "use strict";
    var uuid = require('uuid'),
        dgram = require('dgram'),
        et = require('elementtree'),
        request = require('request');
    
    function DPWSBrowser(config){
        RED.nodes.createNode(this,config);
        
        var node = this;
        var version = config.version;
        var maxDelay = config.maxDelay || 500;
        
        var devices;
        var Devices2Send;
        
        this.on('input', function(msg){
            
            node.devices = [];
            node.Devices2Send = [];
            
            var messageId = genMessageId();
            var body = makeDiscoveryBody(version, messageId);
            
            this.socket = dgram.createSocket('udp4');
            this.socket.on('error', function(err){
                console.log(err); 
            });
            this.socket.on('message', function(msg){
                
                var tree = et.parse(msg.toString());
                var relatesTo = tree.findtext('*/wsa:RelatesTo');
                
                if (relatesTo === messageId) {
                    var matches = tree.findall('*/*/wsd:ProbeMatch');
                    
                    matches.forEach(function (match) {
                        var device = {
                            address: match.findtext('*/wsa:Address'),
                            metadataVersion : match.findtext('wsd:MetadataVersion')
                        }
                        
                        if(existingDevice(node.devices, device)){
                            
                            var index = getDeviceIndex(node.devices, device.address);
                            if(node.devices[index].metadataVersion < device.metadataVersion){

                                //device.types = match.findtext('wsd:Types');
                                device.xaddrs = match.findtext('wsd:XAddrs');
                                node.devices[index] = device;

                                updateDeviceInfos( node, device);
                            }
                                
                        }
                        else{
                            //device.types = match.findtext('wsd:Types');
                            device.xaddrs = match.findtext('wsd:XAddrs');
                            node.devices.push(device);
                            
                            updateDeviceInfos( node, device);
                        }
                      })
                }
            });
            
            this.socket.send(body, 3702, '239.255.255.250');
        });
        
        
    }
    
    RED.nodes.registerType("DPWS Browse", DPWSBrowser);
    
    DPWSBrowser.prototype.close = function(){
        
        if(this.socket){
        
            this.socket.once('close', this.emit.bind(this, 'close'));
            this.socket.close();
        }
    }
    
    function existingDevice(deviceList, device){
        for(var i=0; i< deviceList.length; i++){
            if(deviceList[i].address == device.address){
                return true;
            }
        }
        return false;      
    }
    
    function genMessageId() {
      return 'urn:uuid:' + uuid.v4()
    }

    var PROBE_ACTION = {
      '1.1': 'http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/Probe',
      '2006': 'http://schemas.xmlsoap.org/ws/2005/04/discovery/Probe'
    }
    
    var DISCOVERY_BODY = {
      '1.1':            ['<?xml version="1.0" encoding="UTF-8"?>' +
                        '<s12:Envelope xmlns:dpws="http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01" xmlns:s12="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns:wsd="http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01">' +
                        '<s12:Header>' +
                        '    <wsa:Action>http://docs.oasis-open.org/ws-dd/ns/discovery/2009/01/Probe</wsa:Action>' +
                        '    <wsa:MessageID>',
                        '</wsa:MessageID>' +
                        '    <wsa:To>urn:docs-oasis-open-org:ws-dd:ns:discovery:2009:01</wsa:To>' +
                        '</s12:Header>' +
                        '<s12:Body>' +
                        '    <wsd:Probe/>' +
                        '</s12:Body>' +
                        '</s12:Envelope>'],
      '2006':           ['<?xml version="1.0" encoding="UTF-8"?>' +
                         '<s12:Envelope xmlns:wsdp="http://schemas.xmlsoap.org/ws/2006/02/devprof" xmlns:s12="http://www.w3.org/2003/05/soap-envelope" xmlns:wsa="http://schemas.xmlsoap.org/ws/2004/08/addressing" xmlns:wsd="http://schemas.xmlsoap.org/ws/2005/04/discovery">' +
                         '<s12:Header>' +
                         '    <wsa:Action>http://schemas.xmlsoap.org/ws/2005/04/discovery/Probe</wsa:Action>' +
                         '   <wsa:MessageID>',
                         '</wsa:MessageID>' +
                         '    <wsa:To>urn:schemas-xmlsoap-org:ws:2005:04:discovery</wsa:To>' +
                         '</s12:Header>' +
                         '<s12:Body>' +
                         '    <wsd:Probe/>' +
                         '</s12:Body>' +
                         '</s12:Envelope>']
    }
    
    function makeDiscoveryBody(ver, msgId) {
      var body = DISCOVERY_BODY[ver];

      return new Buffer(body[0] + msgId + body[1]);
    }
    
    function makeGetBody(infos){
        
        return '<?xml version="1.0" encoding="UTF-8"?>'+
        '<s12:Envelope'+
                      ' xmlns:dpws="http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01"' + 
                      ' xmlns:s12="http://www.w3.org/2003/05/soap-envelope"' +
                      ' xmlns:wsa="http://www.w3.org/2005/08/addressing">' +
            '<s12:Header>' +
                '<wsa:Action>http://schemas.xmlsoap.org/ws/2004/09/transfer/Get</wsa:Action>' +
                '<wsa:MessageID>' + infos.messageID + '</wsa:MessageID>' +
                '<wsa:To>' + infos.to + '</wsa:To>' +
            '</s12:Header>' +
            '<s12:Body />' +
        '</s12:Envelope>';
    }
    
    function makeGetMetadataBody(infos){
        
        return  '<?xml version="1.0" encoding="UTF-8"?>' +
                '<s12:Envelope' + 
                    ' xmlns:dpws="http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01"' + 
                    ' xmlns:s12="http://www.w3.org/2003/05/soap-envelope"' + 
                    ' xmlns:wsa="http://www.w3.org/2005/08/addressing"' + 
                    ' xmlns:wsx="http://schemas.xmlsoap.org/ws/2004/09/mex">' +
                    '<s12:Header>' +
                        '<wsa:Action>http://schemas.xmlsoap.org/ws/2004/09/mex/GetMetadata/Request</wsa:Action>' +
                        '<wsa:MessageID>' + infos.messageID + '</wsa:MessageID>' +
                        '<wsa:To>' + infos.to + '</wsa:To>' +
                    '</s12:Header>' +
                    '<s12:Body>' +
                        '<wsx:GetMetadata />' +
                    '</s12:Body>'+
                '</s12:Envelope>';
    }
    
    function getDeviceIndex(devices, deviceID){
        
        for(var i = 0 ; i < devices.length ; i++){
            if(devices[i].address == deviceID){
                return i;
            }
        }
        return -1;
    }
    
    function ipV4Version(address){
        //string pattern to detect 0-8 caracters (https://), 3 times (0-255.) and 1 time (0-255) followed by : and some port number
        return ( address.search("^.{0,8}((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?):[0-9]{1,}")>-1 ? true:false);
    }
    
    function updateDeviceInfos(node, device){
        
        var uri = device.xaddrs;
        
        var infos = {
            messageID: genMessageId(),
            to : device.address
        }
        var xml = makeGetBody(infos);
        request({
        uri: uri,
        method: 'POST',
        body: xml,
        headers: {'content-type':'application/soap+xml'}
        }, function (err, resp, body) {
            if (err) {
              //return callback(err);
            }
            
            if(resp.statusCode >= 400){
                return console.log("Error in Get message");
            }
            
            var tree = et.parse(body);
            
            var hostes = tree.findall('*/*/*/*/dpws:Host');
            var deviceID;
            
            hostes.forEach(function(host){
              
                deviceID = host.findtext('*/wsa:Address');
            })
            
            var dIndex = getDeviceIndex(node.devices, deviceID);
            
            if(dIndex > -1){
                
                var hosted = tree.findall('*/*/*/*/dpws:Hosted');
                node.devices[dIndex].numberOfUnknownServices = hosted.length;
                node.devices[dIndex].friendlyname = tree.findtext('*/*/*/*/dpws:FriendlyName');
                node.devices[dIndex].manufacturer = tree.findtext('*/*/*/*/dpws:Manufacturer');
                node.devices[dIndex].modelName = tree.findtext('*/*/*/*/dpws:ModelName');
                node.devices[dIndex].modelUrl = tree.findtext('*/*/*/*/dpws:ModelUrl');
                node.devices[dIndex].Services = [];
                
                hosted.forEach(function(service){
                    
                    var addresses = service.findall('*/wsa:Address');
                    var serviceId = service.findtext('dpws:ServiceId');
                    node.devices[dIndex].Services.push({serviceID : serviceId, Operations : []});
                    var sIndex = node.devices[dIndex].Services.length-1;
                    
                    addresses.forEach(function(address){
                        
                        if(ipV4Version(address.text)){
                            getMetaData(node, dIndex, sIndex, address.text);
                        }else{
                            //ignored IPV6
                            //console.log("IPV6 address")
                        }
                    })
                })
                
            }
        })
        
    }
    
    function getMetaData(node, dIndex, sIndex, address){
        
        var uri = address;
        
        var infos = {
            messageID: genMessageId(),
            to : address
        }
        var xml = makeGetMetadataBody(infos);
        request({
        uri: uri,
        method: 'POST',
        body: xml,
        headers: {'content-type':'application/soap+xml'}
        }, function (err, resp, body) {
            
            if (err) {
              console.log(err)
                //return callback(err);
            }
            
            if(resp.statusCode >= 400){
                return console.log("Error in Get message");
            }
            
            var tree = et.parse(body);
            var wsdlInfo = tree.findall('*/*/*/wsdl:definitions');
            
            var endpointReferences = tree.findall('*/*/*/*/dpws:Hosted/wsa:EndpointReference');
            var location = "No ipv4 version of location";
            
            endpointReferences.forEach(function(endpointRef){
                
                var aux = endpointRef.findtext('wsa:Address');
                if(ipV4Version(aux)){
                    location = aux;
                }
            });
            
            if(wsdlInfo.length > 0){
                wsdlInfo.forEach(function(wsdl){
                    setWSDLInfos(node, dIndex, sIndex, location, wsdl);
                });
                
                sendInfo(node, dIndex, sIndex);
        }else{
                var wsdlInfoR = tree.findtext('*/*/*/wsx:Location');
                if(wsdlInfoR){
                    request({
                        uri: wsdlInfoR,
                        method: 'GET'
                    }, function (err, resp, body) {
                        
                        if (err) {
                          console.log(err)
                            //return callback(err);
                        }

                        if(resp.statusCode >= 400){
                            return console.log("Error in Get message");
                        }
                        
                        var wsdl = et.parse(body);
                        setWSDLInfos(node, dIndex, sIndex, location, wsdl._root);
                        
                        
                        sendInfo(node, dIndex, sIndex);
                    });
                }else{
                    console.log("can't find the wsdl infos");
                }                
            }
        })
        
    }
    
    function setWSDLInfos(node, dIndex, sIndex, location, wsdl){
        var portTypes = wsdl.findall('wsdl:portType');
                    
        portTypes.forEach(function(portType){

            var operations = portType.findall('wsdl:operation');

            operations.forEach(function(op){

                var inputs = op.findall('wsdl:input');
                var operation;
                
                if(inputs.length > 0){

                    inputs.forEach(function(input){
                        //var action = input.attrib['wsam:Action'];
                        
                        operation = {
                            name: op.attrib['name'],
                            action: input.attrib['wsam:Action'],
                            to: location,
                            type: "operation"
                        }
                    });
                }else{
                    
                    operation = {
                        name: op.attrib['name'],
                        action: wsdl.attrib['targetNamespace'] + '/' + portType.attrib['name'] + '/' + op.attrib['name'],
                        to: location,
                        type: "event"
                    }
                }
                
                //put the operation in the right Service
                node.devices[dIndex].Services[sIndex].Operations.push(operation);

            });
        });
    }
    
    function sendInfo(node, dIndex){
        
        node.devices[dIndex].numberOfUnknownServices--;
        
        //if all services are known
        if(node.devices[dIndex].numberOfUnknownServices == 0){
            
            var device = node.devices[dIndex];
            
            var friendlyname = device.friendlyname;
            
            var newDevice = {
                Name: friendlyname,
                Services: []
            }
            
            device.Services.forEach(function(service){
                var newService = {ServiceID: service.serviceID, Operations: []};
                
                service.Operations.forEach(function(operation){
                   newService.Operations.push(operation); 
                });
                
                newDevice.Services.push(newService);
            });
            
            node.Devices2Send.push(newDevice);
            
            var msg = {payload: node.Devices2Send};
            
            node.send(msg);
        }
    }
}