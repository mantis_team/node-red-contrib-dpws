module.exports = function(RED){
    "use strict";
    var request = require('request')
      , et = require('elementtree')
      , uuid = require('uuid')
      , http = require('http')
      , OS = require('os')
      , net = require('net');
    
    function DPWSConnector(config){
        RED.nodes.createNode(this,config);
        
        this.status({fill:"blue", shape:"ring", text:"Ready"});
        
        var node = this;
        var hasServer = false;
        var serverInfos = {};
        var server;
        
        this.on('input', function(msg){
            
            var opts = {
                action: config.action,
                to : config.to,
                expires: config.expires
            }
            
            if(config.parameters){
                if(msg.payload)
                    opts.parameters = msg.payload;
                else{
                    this.log("must define the input parameters or disable the checkbox!!");
                    return;
                }
            }
            
            var request_type = config.request_type;
            
            if(msg.payload.requestType){
                request_type = msg.payload.requestType;
                opts.expires = 0;
            }
            if(msg.payload.action){
                opts.action = msg.payload.action;
            }
            if(msg.payload.to){
                opts.to = msg.payload.to;
            }
            if(msg.payload.expires){
                opts.expires = msg.payload.expires;
            }
            
            if(!this.hasServer){
                
                switch(request_type){

                    case "invoke":

                        makeTheRequest(opts,function( err, msgSent, msgResp){

                            var response = "error!! - default";
                            var message = {};
                            
                            if(err){
                                
                                message.payload = err;
                                node.status({fill:"red", shape:"dot", text:"Invocation failed"});
                                
                                node.send(message);
                            }else{
                                response = msgResp
                                node.status({fill:"green", shape:"dot", text:"Successfully invoked"});
                                
                                message.payload = response.toString();
                                //send the result to the next node
                                node.send(message);
                            }

                            setTimeout(function(){node.status({fill:"blue", shape:"ring", text:"Ready"})}, 2000);
                        });
                        break;

                    case "subscribe":
                        makeTheSubscriptionRequest(this, opts, function( err, msgSent, msgResp){

                            if(err){
                                node.status({fill:"red", shape:"dot", text:"Subscription failed"});
                                setTimeout(function(){node.status({fill:"blue", shape:"ring", text:"Ready"})}, 2000);                                
                                stopServer(node);
                                var message = {};
                                message.payload = err;
                                node.send(message);
                            }else{
                                
                                if(opts.expires > 0)
                                    setTimeout(function(){
                                        stopServer(node);
                                        node.status({fill:"blue", shape:"ring", text:"Ready"});
                                    },parseInt(opts.expires)*1000);
                                
                                node.status({fill:"green", shape:"dot", text:"Successfully subscribed"});
                            }
                        });
                        break;
                }
            }
            else{
                makeTheUnsubscriptionRequest(node, opts, function( err, msgSent, msgResp){

                        if(err){
                            node.status({fill:"red", shape:"dot", text:"An error has occurred"});
                        }else{

                            node.status({fill:"blue", shape:"dot", text:"Unsubscribe properly"});
                        }
                        setTimeout(function(){node.status({fill:"blue", shape:"ring", text:"Ready"})}, 2000);
                });    
            }
        });
    }
    
    RED.nodes.registerType("DPWS Client",DPWSConnector);

    DPWSConnector.prototype.close = function(){
        
        var node = this;
        
        if(this.hasServer){
                
            makeTheUnsubscriptionRequest(node, node.opts, function( err, msgSent, msgResp){

                    if(err){
                        node.status({fill:"red", shape:"dot", text:"An error has occurred"});
                    }else{

                        node.status({fill:"blue", shape:"dot", text:"Unsubscribe properly"});
                    }
                 
                    setTimeout(function(){node.status({fill:"grey", shape:"dot", text:"Offline"})}, 2000);
                });
        }
        
        this.status({fill:"gray", shape:"dot", text:"Offline"});
    }
    
    
    //randomly generate a uuid
    function genMessageId() {
      return 'urn:uuid:' + uuid.v4()
    }
    
    function stopServer(node){
        //console.log("Stopping server -> "+node.serverInfos.address+":"+node.serverInfos.port);
        node.server.close();
        node.hasServer = false;
        node.serverInfos = {};
    }
    
    //get a valid port to launch the server
    var portrange = 45032;
    function getPort (cb) {

        var port = portrange;
        portrange += 1;
        
        var server2 = net.createServer();

        server2.listen(port, function (err) {
            server2.once('close', function () {
                cb(port);
            });
            server2.close();
        });
        server2.on('error', function (err) {
            getPort(cb);
        });
    }
    
    function makeSubscriptionBody(opts){
        
        var expires = "";
        if(opts.expires > 0){
            expires = "<wse:Expires>PT"+opts.expires+"S</wse:Expires>";
        }
        
        
      return '<?xml version="1.0" encoding="UTF-8"?>' +
      '<s12:Envelope' +
          ' xmlns:dpws="http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01"' +
          ' xmlns:s12="http://www.w3.org/2003/05/soap-envelope"' +
          ' xmlns:wsa="http://www.w3.org/2005/08/addressing"' +
          ' xmlns:wse="http://schemas.xmlsoap.org/ws/2004/08/eventing"' +
          '>' +
          '<s12:Header>' +
              '<wsa:Action>http://schemas.xmlsoap.org/ws/2004/08/eventing/Subscribe</wsa:Action>' +
              '<wsa:MessageID>' + opts.messageId + '</wsa:MessageID>' +
              '<wsa:To>' + opts.to + '</wsa:To>' +
          '</s12:Header>'+
          '<s12:Body>' +
              '<wse:Subscribe>' +
                 '<wse:Delivery Mode="http://schemas.xmlsoap.org/ws/2004/08/eventing/DeliveryModes/Push">' +
                    '<wse:NotifyTo> '+
                        '<wsa:Address>http://'+opts.server.address+':'+opts.server.port+'/'+opts.server.topic +'</wsa:Address>'+
                        '<wsa:ReferenceParameters>'+
                            '<wse:Identifier>' + opts.messageId + '</wse:Identifier>'+
                        '</wsa:ReferenceParameters>'+
                    '</wse:NotifyTo>'+
                '</wse:Delivery>'+
                expires +
                '<wse:Filter Dialect="http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01/Action">'+
                    opts.action +
                '</wse:Filter>'+
              '</wse:Subscribe>'+
         '</s12:Body>'+
      '</s12:Envelope>';
    }
    
    function makeUnsubscriptionBody(node, opts){
        
      return '<?xml version="1.0" encoding="UTF-8"?>' +
      '<s12:Envelope' +
          ' xmlns:dpws="http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01"' +
          ' xmlns:s12="http://www.w3.org/2003/05/soap-envelope"' +
          ' xmlns:wsa="http://www.w3.org/2005/08/addressing"' +
          ' xmlns:wse="http://schemas.xmlsoap.org/ws/2004/08/eventing"' +
          '>' +
          '<s12:Header>' +
              '<wsa:Action>http://schemas.xmlsoap.org/ws/2004/08/eventing/Unsubscribe</wsa:Action>' +
              '<wsa:MessageID>' + opts.messageId + '</wsa:MessageID>' +
              '<wsa:To>' + opts.to + '</wsa:To>' +
              '<wse:Identifier wsa:IsReferenceParameter="true">'+ node.serverInfos.ServRef +'</wse:Identifier>'+
          '</s12:Header>'+
          '<s12:Body>' +
              '<wse:Unsubscribe />' +
         '</s12:Body>'+
      '</s12:Envelope>';
    }
    
    function makeInvocationBody(opts) {
        
        var body = "<s12:Body/>";
    
        if(opts.parameters){
            body = "<s12:Body>"+ opts.parameters+"</s12:Body>"
        }
        
        
      return '<?xml version="1.0" encoding="UTF-8"?>' +
      '<s12:Envelope' +
          ' xmlns:dpws="http://docs.oasis-open.org/ws-dd/ns/dpws/2009/01"' +
          ' xmlns:s12="http://www.w3.org/2003/05/soap-envelope"' +
          ' xmlns:wsa="http://www.w3.org/2005/08/addressing"' +
          '>' +
      '<s12:Header>' +
          '<wsa:Action>' + opts.action + '</wsa:Action>' +
          '<wsa:MessageID>' + opts.messageId + '</wsa:MessageID>' +
          '<wsa:To>' + opts.to + '</wsa:To>' +
      '</s12:Header>' +
        body+
      '</s12:Envelope>'
    }
  
    function getSubscriptionMessage(node, opts, callback){
        
        //get the ipv4 address
        var interfaces = OS.networkInterfaces();
        var addresses = [];
        for (var k in interfaces) {
            for (var k2 in interfaces[k]) {
                var address = interfaces[k][k2];
                if (address.family === 'IPv4' && !address.internal) {
                    addresses.push(address.address);
                }
            }
        }
        
        //create a server to listen to subscribe responses
        node.server = http.createServer(function(request, response){
            
            var method = request.method;
            var topic = request.url.slice(1);
            var body = [];
            
            if(method == "POST" && topic == node.serverInfos.topic)
            {
                
                //get the body of the received message
                request.on('error', function(err){
                    console.error(err);
                }).on('data', function(chunk){
                    body.push(chunk);
                }).on('end',function(){
                    
                    var bodyAsString = body.toString();
                    var content = et.parse(bodyAsString).find('s12:Body')
                      , reply = [];
                    if (content) {
                      for (var i = 0; i < content._children.length; i++) {
                        var child = content._children[i];
                        reply.push(child.text);
                      }
                    }else{
                        reply = "Error occured";
                    }
                    
                    var message = {};
                    message.payload = reply.toString();
                    
                    //send the result to the next node
                    node.send(message);
                    
                    response.statusCode = 202;
                    response.setHeader('Connection','keep-alive');
                    response.end();
                });
                
            }else{
                response.statusCode = 400;
                response.end();
            }
        });
        
        //get a valid port and put the server listening in the local IPV4 address and with this port
        getPort(function( portValue){
            node.server.listen(portValue,addresses[0]);
            //console.log("\nServer listening in: \"" +addresses[0]+":"+portValue+"\"\n");

            opts.messageId = opts.messageId || genMessageId();

            opts.server = {
                address: addresses[0],
                port: portValue,
                topic: opts.messageId.slice(9)
            }
            node.serverInfos = opts.server;
            node.hasServer = true;

            callback(null,makeSubscriptionBody(opts));
            
        });
    }
    
    function makeTheUnsubscriptionRequest(node, opts, callback){
        opts.messageId = opts.messageId || genMessageId();

        var uri = opts.to || opts.uri;

        var xml = makeUnsubscriptionBody(node, opts);
        
        request({
        uri: uri,
        method: 'POST',
        body: xml,
        headers: {'content-type':'application/soap+xml','connection':'close'}
        }, function (err, resp, body) {
            
        if (err) {
          return callback(err)
        }

        if (resp.statusCode >= 400) {
            return callback(body || ("Request status code : " + resp.statusCode));
            //throw new Error(body || "request error")
        }

        var body = et.parse(body).find('s12:Body')
          , reply = []
        if (body) {
          for (var i = 0; i < body._children.length; i++) {
            var child = body._children[i]

            reply.push(child.text)
          }
        }
        
        stopServer(node);
            
        callback(null, xml, reply)
        })
    }
    
    function makeTheSubscriptionRequest(node, options, callback){
        getSubscriptionMessage(node, options,function(error, message){
            
            var uri = options.to || options.uri;
            
            if(!error){
                request({
                uri: uri,
                method: 'POST',
                body: message,
                headers: {'content-type':'application/soap+xml'}
                }, function (err, resp, body) {
                if (err) {
                    return callback(err);
                }

                if (resp.statusCode >= 400) {
                    return callback(body || ("Request status code : " + resp.statusCode));
                    //throw new Error(body || "request error")
                }

                var body2 = et.parse(body).find('s12:Body')
                    , reply = [];
                if (body2) {
                  for (var i = 0; i < body2._children.length; i++) {
                    var child = body2._children[i];
                    reply.push(child.text)
                  }
                }
                    
                node.serverInfos.ServRef = body2.find('wse:SubscribeResponse').find('wse:SubscriptionManager').find('wsa:ReferenceParameters')
                                                    .find('wse:Identifier').text;

                callback(null, message, reply);
                });
                
            }else{
                callback(error);                   
            }
        });
    }
    
    function makeTheRequest(opts, callback){
        opts.messageId = opts.messageId || genMessageId();

        var uri = opts.to || opts.uri;

        var xml = makeInvocationBody(opts);
        
        request({
        uri: uri,
        method: 'POST',
        body: xml,
        headers: {'content-type':'application/soap+xml'}
        }, function (err, resp, body) {
        if (err) {
          return callback(err)
        }

        if (resp.statusCode >= 400) {
            return callback(body || ("Request status code : " + resp.statusCode));
            //throw new Error(body || "request error")
        }

        var body = et.parse(body).find('s12:Body')
          , reply = []
        if (body) {
          for (var i = 0; i < body._children.length; i++) {
            var child = body._children[i]

            reply.push(child.text)
          }
        }

        callback(null, xml, reply)
        })
    }
}