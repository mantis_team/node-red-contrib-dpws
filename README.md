


node-red-contrib-dpws
========================

A [Node-RED](http://nodered.org) node to subscribe or invoke via [DPWS](http://ws4d.org/technology/dpws/) standart.

based on [node-dpws](https://github.com/danmilon/node-dpws)

Install
-------

Run command on Node-RED installation directory.

	npm install node-red-contrib-dpws

or run command for global installation.

	npm install -g node-red-contrib-dpws

Usage
-----
    
This is a simple example of using DPWS-Client to interact with an external device, and print the response on the debug tab.

![node-red-dpws-flow](https://bytebucket.org/mantis_team/node-red-contrib-dpws/raw/0d717e3cd549988037795003641dfaba575bbc1c/Images/DPWS%20usage%20image.png)
    
    
# Authors

* since 2017 [Pedro Barroca](https://bitbucket.org/PedroBarroca/)
* since 2017 [Giovanni Di Orio](https://bitbucket.org/giannidiorio/)

    